/*
  A library for driving bidirectional dc motors.
  By Aidan Taylor 2018

  This code is in the public domain
*/

#ifndef BiDCMotor_h
#define BiDCMotor_h

#include "Arduino.h"

class BiDCMotor {
public:
  BiDCMotor(uint8_t pin0, uint8_t pin1, uint8_t enablePin);
  void run(bool direction, uint8_t speed);
  void stop();
private:
  uint8_t _pin0, _pin1, _enablePin;
  bool _direction;
  uint8_t _speed;
};

#endif
