/*
  A library for driving bidirectional dc motors. Pin 0 and 1 should be PWM
  capable.

  By Aidan Taylor 2018

  This code is in the public domain
*/

#include "Arduino.h"
#include "BiDCMotor.h"

BiDCMotor::BiDCMotor(uint8_t pin0, uint8_t pin1, uint8_t enablePin) {
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(enablePin, OUTPUT);

  _pin0 = pin0;
  _pin1 = pin1;
  _enablePin = enablePin;
}

void BiDCMotor::run(bool direction, uint8_t speed) {
  _direction = direction;
  _speed = speed;

  if(_direction) {
    analogWrite(_pin0, _speed);
    digitalWrite(_pin1, LOW);
  }
  else if(!_direction) {
    digitalWrite(_pin0, LOW);
    analogWrite(_pin1, _speed);
  }
  digitalWrite(_enablePin, HIGH);
}

void BiDCMotor::stop() {
  digitalWrite(_pin0, LOW);
  digitalWrite(_pin1, LOW);
  digitalWrite(_enablePin, LOW);
}
