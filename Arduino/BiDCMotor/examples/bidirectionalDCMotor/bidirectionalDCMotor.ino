/*
  Bidirectional DC Motor driving library by Aidan Taylor. The library provides
  a simple class that makes driving a DC motor with a chip such as the SN754410
  or a close equivalent easy.

  This example demonstrates usage of the BiDCMotor object.

  Aidan Taylor. 2018.

  Please see the license file included in my kinetics
  repository for usage (tldr: it's a GNU Public v3 license)
*/

#include "BiDCMotor.h"

// Create an instance of BiDCMotor
// Define the pins used on your device - the format is (motorA, motorB, Enable)
// motorA and motorB pins should be PWM capable.
BiDCMotor myMotor(3, 5, 2);

void setup() {

}

// Motor commands:
void loop() {
  // Move the motor forwards:
  myMotor.run(1, 255); // (direction, speed) 255 is max speed, one byte resolution

  delay(1000);

  // Move the motor backwards at about half speed:
  myMotor.run(0, 127);

  delay(1000);

  // Stop the motor:
  myMotor.stop();

  delay(1000);
}
