/*
  A library for driving unipolar stepper motors.
  By Aidan Taylor 2018

  This code is in the public domain
*/

#include "Arduino.h"
#include "UniStepper.h"

// Initialiser function:
UniStepper::UniStepper(uint8_t pin0, uint8_t pin1, uint8_t pin2, uint8_t pin3) {
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);

  _pin0 = pin0;
  _pin1 = pin1;
  _pin2 = pin2;
  _pin3 = pin3;

  _counter = 0;
}

void UniStepper::incStep(bool increment) {
  _increment = increment;

  if(_increment) {
    if(_counter == 4) _counter = 0;
    else if(_counter == 255) _counter = 3;

    if(_counter == 0) {
      digitalWrite(_pin0, HIGH);
      digitalWrite(_pin1, LOW);
      digitalWrite(_pin2, LOW);
      digitalWrite(_pin3, HIGH);
    }
    else if(_counter == 1) {
      digitalWrite(_pin0, HIGH);
      digitalWrite(_pin1, HIGH);
      digitalWrite(_pin2, LOW);
      digitalWrite(_pin3, LOW);
    }
    else if(_counter == 2) {
      digitalWrite(_pin0, LOW);
      digitalWrite(_pin1, HIGH);
      digitalWrite(_pin2, HIGH);
      digitalWrite(_pin3, LOW);
    }
    else if(_counter == 3) {
      digitalWrite(_pin0, LOW);
      digitalWrite(_pin1, LOW);
      digitalWrite(_pin2, HIGH);
      digitalWrite(_pin3, HIGH);
    }

    _counter++;
  }
  else if(!_increment) {
    if(_counter == 4) _counter = 0;
    else if(_counter == 255) _counter = 3;

    if(_counter == 0) {
      digitalWrite(_pin0, HIGH);
      digitalWrite(_pin1, LOW);
      digitalWrite(_pin2, LOW);
      digitalWrite(_pin3, HIGH);
    }
    else if(_counter == 1) {
      digitalWrite(_pin0, HIGH);
      digitalWrite(_pin1, HIGH);
      digitalWrite(_pin2, LOW);
      digitalWrite(_pin3, LOW);
    }
    else if(_counter == 2) {
      digitalWrite(_pin0, LOW);
      digitalWrite(_pin1, HIGH);
      digitalWrite(_pin2, HIGH);
      digitalWrite(_pin3, LOW);
    }
    else if(_counter == 3) {
      digitalWrite(_pin0, LOW);
      digitalWrite(_pin1, LOW);
      digitalWrite(_pin2, HIGH);
      digitalWrite(_pin3, HIGH);
    }

    _counter--;
  }
}
