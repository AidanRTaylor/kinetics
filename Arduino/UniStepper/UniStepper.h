/*
  A library for driving unipolar stepper motors.
  By Aidan Taylor 2018

  This code is in the public domain
*/

#ifndef UniStepper_h
#define UniStepper_h

#include "Arduino.h"

class UniStepper {
public:
  UniStepper(uint8_t pin0, uint8_t pin1, uint8_t pin2, uint8_t pin3);
  void incStep(bool increment);
private:
  uint8_t _pin0, _pin1, _pin2, _pin3;
  bool _increment;
  uint8_t _counter;
};

#endif
