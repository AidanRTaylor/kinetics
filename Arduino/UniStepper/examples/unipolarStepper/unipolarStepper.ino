/*
  Unipolar stepper motor driving library by Aidan Taylor. The library provides
  a simple class that makes driving a DC motor with a chip such as the uln2003
  or a close equivalent easy.

  This example demonstrates usage of the UniStepper object.

  Aidan Taylor. 2018.

  Please see the license file included in my kinetics
  repository for usage (tldr: it's a GNU Public v3 license)
*/

#include "UniStepper.h"

// Create an instance of UniStepper
// Define the 4 pins used for driving your device
UniStepper myStepper(2, 4, 7, 8);

void setup() {

}

// Motor commands:
void loop() {
  // Increment the stepper forwards once:
  myStepper.incStep(1);

  delay(1000);

  // Increment the stepper backwards once:
  myStepper.incStep(0);

  delay(1000);

  // Use a for loop to increment the stepper a set amount
  // (forwards 100 steps):
  for(uint8_t n = 0; n < 100; n++) {
    myStepper.incStep(1);
    delay(100); // depending on the stepper, this will be quite slow
  }

  delay(1000);

  // Use a for loop to increment the stepper a set amount
  // (reverse 100 steps):
  for(uint8_t n = 0; n < 100; n++) {
    myStepper.incStep(0);
    delay(100);
  }

  delay(1000);

}
