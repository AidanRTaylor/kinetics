# Kinetics by Aidan Taylor

Kinetics is a set of classes for driving various kinds of motors. I am currently working on C++ classes for the Arduino platform, but want to add equivalents for Micropython as soon as possible. 

There is nothing majorly complicated about these provided libraries, in fact it is perhaps an over complication on my part. I made the project to teach myself about classes and libraries in C++ and Python3! (Still, they are functional, fast to implement, clean and simple so I think they can be useful!!) 

Please see the included License file for usage - you are free to do as you wish with this code. You alone are responsible for any applications that involve use of this repository. 

Contributions welcome, please contact me!

## Arduino Use:

Clone or download the repository. Once downloaded, find the Arduino directory in the repository and copy any or all of the sub-folders to your **Arduino libraries** folder. 

Usage examples are included with each library.  
